import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // super hit key check
    const crit = [];
    // super hit cooldown check
    let superHitCdPlayerRight = false;
    let superHitCdPlayerLeft = false;

    document.addEventListener('keypress', (e) => {
      const key = e.code;
      // check keys for super hit
      if (crit.length < 3) {
        crit.push(key);
      } else if (crit.length >= 3) {
        crit.shift();
        crit.push(key);
      }

      // check for player one hit and is not in block
      if (
        key === controls.PlayerOneAttack &&
        crit[1] !== controls.PlayerOneBlock &&
        crit[1] !== controls.PlayerTwoBlock
      ) {
        hbar = 'right';
        getDamage(firstFighter, secondFighter);
      } else if (key === controls.PlayerOneBlock && fightOver === false) {
        console.log('block');
      } else if (
        // check for super hit
        crit[0] === controls.PlayerOneCriticalHitCombination[0] &&
        crit[1] === controls.PlayerOneCriticalHitCombination[1] &&
        crit[2] === controls.PlayerOneCriticalHitCombination[2]
      ) {
        // check if super hit is on cooldown
        if (!superHitCdPlayerLeft && fightOver === false) {
          hbar = 'right';
          const obj = {
            attack: secondFighter.attack * 2,
          };
          const sobj = {
            defense: 0,
          };
          getDamage(obj, sobj);
          console.log('super hit');
          superHitCdPlayerLeft = true;
          setTimeout(() => (superHitCdPlayerLeft = false), 10000);
        } else if (fightOver === false) {
          console.log('cooldown');
        }
      }

      // check for player two hit and is not in block
      if (
        key === controls.PlayerTwoAttack &&
        crit[1] !== controls.PlayerTwoBlock &&
        crit[1] !== controls.PlayerOneBlock
      ) {
        hbar = 'left';
        getDamage(secondFighter, firstFighter);
      } else if (key === controls.PlayerTwoBlock && fightOver === false) {
        console.log('block');
      } else if (
        // check for super hit
        crit[0] === controls.PlayerTwoCriticalHitCombination[0] &&
        crit[1] === controls.PlayerTwoCriticalHitCombination[1] &&
        crit[2] === controls.PlayerTwoCriticalHitCombination[2]
      ) {
        // check if super hit is on cooldown
        if (!superHitCdPlayerRight && fightOver === false) {
          hbar = 'left';
          const obj = {
            attack: secondFighter.attack * 2,
          };
          const sobj = {
            defense: 0,
          };
          getDamage(obj, sobj);
          console.log('super hit');
          superHitCdPlayerRight = true;
          setTimeout(() => (superHitCdPlayerRight = false), 10000);
        } else if (fightOver === false) {
          console.log('cooldown');
        }
      }
      if (fightOver === true) {
        resolve(winner);
      }
    });
  });
}

// for check if fight is over
let fightOver = false;
// for damage to width
let wl = 100;
let wr = 100;
let winner = {};
// health bar param for damage func
let hbar = '';

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  const hpBar =
    hbar === 'right'
      ? document.getElementById('right-fighter-indicator')
      : document.getElementById('left-fighter-indicator');
  if (hbar === 'right') {
    if (Math.sign(damage) > 0 && fightOver === false) {
      wr -= damage;
      if (wr <= damage && fightOver === false) {
        console.log(damage);
        hpBar.style.maxWidth = '0%';
        winner = attacker;
        fightOver = true;
        return damage;
      } else if (fightOver === false) {
        console.log(damage);
        hpBar.style.maxWidth = wr + '%';
        return damage;
      }
    } else if (fightOver === false) {
      console.log('damage 0');
      return 0;
    }
  } else {
    if (Math.sign(damage) > 0 && fightOver === false) {
      wl -= damage;
      if (wl <= damage && fightOver === false) {
        console.log(damage);
        hpBar.style.maxWidth = '0%';
        winner = attacker;
        fightOver = true;
        return damage;
      } else if (fightOver === false) {
        console.log(damage);
        hpBar.style.maxWidth = wl + '%';
        return damage;
      }
    } else if (fightOver === false) {
      console.log('damage 0 or less');
      return 0;
    }
  }
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const randomNumber = Math.random() * 1 + 1;
  return randomNumber * attack;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const randomNumber = Math.random() * 1 + 1;
  return randomNumber * defense;
}

import { createElement } from '../helpers/domHelper';
import { fighterDetailsMap } from '../components/fighterSelector';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const ul = document.createElement('ul');
  ul.style.listStyleType = 'none';
  ul.style.color = 'lightblue';
  ul.style.fontWeight = 'bold';
  ul.style.fontSize = '20px';

  const name = document.createElement('li');
  name.textContent = fighterDetailsMap.get('name');
  ul.appendChild(name);

  const health = document.createElement('li');
  health.textContent = 'Health ' + fighterDetailsMap.get('health');
  ul.appendChild(health);

  const attack = document.createElement('li');
  attack.textContent = 'attack ' + fighterDetailsMap.get('attack');
  ul.appendChild(attack);

  const defense = document.createElement('li');
  defense.textContent = 'defense ' + fighterDetailsMap.get('defense');
  ul.appendChild(defense);

  fighterElement.appendChild(ul);
  const obj = {
    name: fighterDetailsMap.get('name'),
    source: fighterDetailsMap.get('source'),
    alt: fighterDetailsMap.get('name'),
  };
  const x = createFighterImage(obj);
  x.style.maxHeight = ' 200px';
  fighterElement.appendChild(x);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
